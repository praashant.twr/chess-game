from tkinter import *
from pieces import validator
from initialize import *

root = Tk()
root.geometry('800x600')

selected_piece = StringVar()
placing = BooleanVar()

b_pawn_img = PhotoImage(file='pieces_img/bpawn.ppm')
b_king_img = PhotoImage(file='pieces_img/bking.ppm')
b_queen_img = PhotoImage(file='pieces_img/bqueen.ppm')
b_bishop_img = PhotoImage(file='pieces_img/bbishop.ppm')
b_rook_img = PhotoImage(file='pieces_img/brook.ppm')
b_knight_img = PhotoImage(file='pieces_img/bknight.ppm')

w_pawn_img = PhotoImage(file='pieces_img/wpawn.ppm')
w_king_img = PhotoImage(file='pieces_img/wking.ppm')
w_queen_img = PhotoImage(file='pieces_img/wqueen.ppm')
w_bishop_img = PhotoImage(file='pieces_img/wbishop.ppm')
w_rook_img = PhotoImage(file='pieces_img/wrook.ppm')
w_knight_img = PhotoImage(file='pieces_img/wknight.ppm')

position_x = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g', 8: 'h'}

class Board:
    def __init__(self, presence, pos_x, pos_y, color, piece_color, piece_name=None, piece=None):
        self.piece_color = piece_color
        self.frame_color = color
        self.piece_is_present = presence
        self.x_index = pos_x
        self.y_index = pos_y
        self.piece = piece
        self.piece_name = piece_name
    
    def next_click(self):
        name = board_frames[selected_piece.get()].piece_name
        ix = int(selected_piece.get()[0])
        iy = int(selected_piece.get()[1])
        if validator(name, ix, iy, self.x_index, self.y_index):
            self.piece_is_present = True
            self.piece_name = name
            board_frames[selected_piece.get()].piece_name = None
            self.piece_color = board_frames[selected_piece.get()].piece_color
            board_frames[selected_piece.get()].piece_color = None
            self.piece_type(board_frames[selected_piece.get()].piece)
            self.piece = board_frames[selected_piece.get()].piece
        else:
            piece_retype = board_frames[selected_piece.get()].piece
            board_frames[selected_piece.get()].piece_type(piece_retype)
            board_frames[selected_piece.get()].piece_is_present = True

    def movenment(self, event):
        if self.piece_is_present == True:
            if placing.get() == False:
                placing.set(True)
                self.piece_is_present = False
                self.frame()
                selected_piece.set(str(self.x_index)+str(self.y_index))
            else:
                placing.set(False)
                if self.piece_color != board_frames[selected_piece.get()].piece_color:
                    self.next_click()
                else:
                    piece_retype = board_frames[selected_piece.get()].piece
                    board_frames[selected_piece.get()].piece_type(piece_retype)
                    board_frames[selected_piece.get()].piece_is_present = True

        else:
            if placing.get() == True:
                placing.set(False)
                self.next_click()

    def frame(self):
        f = Frame(root, height=60, width=60, background=self.frame_color)
        f.bind('<Button-1>', self.movenment)
        f.grid(row=self.x_index, column=self.y_index)

    def piece_type(self, piece):
        if piece != None:
            Label(root, image=piece).grid(row=self.x_index, column=self.y_index)


def initialize_board():
    placing.set(False)
    for i in range(9):
        for j in range(9):
            frame_name = str(i)+str(j)
            if i == 8 or j == 8:
                if i == 8 and j == 8:
                    pass
                elif j == 8:
                    Label(root, text=i + 1).grid(row=i, column=j)
                elif i == 8:
                    Label(root, text=position_x[j + 1]).grid(row=i, column=j)
            else:
                if i % 2 == 0 and j % 2 == 0:
                    board_frames[frame_name] = Board(False, i, j, 'black', None)
                    board_frames[frame_name].frame()
                elif i % 2 != 0:
                    if j % 2 == 0:
                        board_frames[frame_name] = Board(False, i, j, 'white', None)
                        board_frames[frame_name].frame()
                    else:
                        board_frames[frame_name] = Board(False, i, j, 'black', None)
                        board_frames[frame_name].frame()
                else:
                    board_frames[frame_name] = Board(False, i, j, 'white', None)
                    board_frames[frame_name].frame()

    for i in range(8):
        board_frames['6'+str(i)].piece_color = 'black'
        board_frames['6'+str(i)].piece_is_present = True
        board_frames['6'+str(i)].piece_type(b_pawn_img)
        board_frames['6'+str(i)].piece = b_pawn_img
        board_frames['6'+str(i)].piece_name = 'pawn'

    for i in range(8):
        board_frames['1' + str(i)].piece_color = 'white'
        board_frames['1'+str(i)].piece_is_present = True
        board_frames['1'+str(i)].piece_type(w_pawn_img)
        board_frames['1'+str(i)].piece = w_pawn_img
        board_frames['1'+str(i)].piece_name = 'pawn'
    
    for i in range(8):
        board_frames['0'+str(i)].piece_color = 'white'
        board_frames['0'+str(i)].piece_is_present = True
        if i == 0 or i == 7:
            board_frames['0'+str(i)].piece_type(w_rook_img)
            board_frames['0'+str(i)].piece = w_rook_img
            board_frames['0'+str(i)].piece_name = 'rook'
        elif i == 1 or i == 6:
            board_frames['0'+str(i)].piece_type(w_knight_img)
            board_frames['0'+str(i)].piece = w_knight_img
            board_frames['0'+str(i)].piece_name = 'knight'
        elif i == 2 or i == 5:
            board_frames['0'+str(i)].piece_type(w_bishop_img)
            board_frames['0'+str(i)].piece = w_bishop_img
            board_frames['0'+str(i)].piece_name = 'bishop'
        elif i == 3:
            board_frames['0'+str(i)].piece_type(w_queen_img)
            board_frames['0'+str(i)].piece = w_queen_img
            board_frames['0'+str(i)].piece_name = 'queen'
        else:
            board_frames['0'+str(i)].piece_type(w_king_img)
            board_frames['0'+str(i)].piece = w_king_img
            board_frames['0'+str(i)].piece_name = 'king'
    
    for i in range(8):
        board_frames['7'+str(i)].piece_color = 'black'
        board_frames['7'+str(i)].piece_is_present = True
        if i == 0 or i == 7:
            board_frames['7'+str(i)].piece_type(b_rook_img)
            board_frames['7'+str(i)].piece_name = 'rook'
            board_frames['7'+str(i)].piece = b_rook_img
        elif i == 1 or i == 6:
            board_frames['7'+str(i)].piece_type(b_knight_img)
            board_frames['7'+str(i)].piece = b_knight_img
            board_frames['7'+str(i)].piece_name = 'knight'
        elif i == 2 or i == 5:
            board_frames['7'+str(i)].piece_type(b_bishop_img)
            board_frames['7'+str(i)].piece = b_bishop_img
            board_frames['7'+str(i)].piece_name = 'bishop'
        elif i == 3:
            board_frames['7'+str(i)].piece_type(b_queen_img)
            board_frames['7'+str(i)].piece = b_queen_img
            board_frames['7'+str(i)].piece_name = 'queen'
        else:
            board_frames['7'+str(i)].piece_type(b_king_img)
            board_frames['7'+str(i)].piece = b_king_img
            board_frames['7'+str(i)].piece_name = 'king'

initialize_board()
root.mainloop()
