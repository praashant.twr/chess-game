from initialize import *

def validator(piece_name, ix, iy, fx, fy, limit=None):
    if piece_name == 'rook':
        return Rook().valid_move(ix, iy, fx, fy)
    elif piece_name == 'bishop':
        return Bishop().valid_move(ix, iy, fx, fy)
    elif piece_name == 'king':
        return King().valid_move(ix, iy, fx, fy)
    elif piece_name == 'queen':
        return Queen().valid_move(ix, iy, fx, fy)
    elif piece_name == 'knight':
        return Knight().valid_move(ix, iy, fx, fy)
    elif piece_name == 'pawn':
        return Pawn().valid_move(ix, iy, fx, fy, limit)
    

class Pieces:
    def horizonatal_move(self, initial_x, initial_y, final_x, final_y,  limit=None):
        if initial_x == final_x:
            if limit == None:
                start = min(initial_y, final_y)
                end = max(initial_y, final_y)
                for i in range(start+1, end):
                    location = str(initial_x)+str(i)
                    if board_frames[location].piece_is_present == True:
                        return False
                return True
            elif abs(final_y - initial_y) <= limit:
                start = min(initial_y, final_y)
                end = max(initial_y, final_y)
                for i in range(start+1, end):
                    location = str(initial_x)+str(i)
                    if board_frames[location].piece_is_present == True:
                        return False
                return True
        return False

    def vertical_move(self, initial_x, initial_y, final_x, final_y,  limit=None):
        if initial_y == final_y:
            if limit == None:
                start = min(initial_x, final_x)
                end = max(initial_x, final_x)
                for i in range(start+1, end):
                    location = str(i)+str(initial_y)
                    if board_frames[location].piece_is_present == True:
                        return False
                return True
            elif abs(initial_x - final_x) <= limit:
                start = min(initial_x, final_x)
                end = max(initial_x, final_x)
                for i in range(start+1, end):
                    location = str(i)+str(initial_y)
                    if board_frames[location].piece_is_present == True:
                        return False
                return True
        return False
    
    def diagonal_move(self, initial_x, initial_y, final_x, final_y, limit=None):
        if abs(final_y - initial_y) == abs(final_x - initial_x):
            if limit == None:
                if (initial_x < final_x and initial_y < final_y) or (final_x < initial_x and final_y < initial_y):
                    start_x = min(initial_x, final_x)
                    end_x = max(initial_x, final_x)
                    start_y = min(final_y, initial_y)
                    end_y = max(initial_y, final_y)
                    while start_x+1 < end_x and start_y+1 < end_y:
                        start_x += 1
                        start_y += 1
                        location = str(start_x)+str(start_y)
                        if board_frames[location].piece_is_present == True:
                            return False
                elif (final_x < initial_x and initial_y < final_y) or (initial_x < final_x and final_y < initial_y):
                    start_x = min(initial_x, final_x)
                    end_x = max(initial_x, final_x)
                    start_y = max(final_y, initial_y)
                    end_y = min(initial_y, final_y)
                    while end_x > start_x+1 and start_y > end_y+1:
                        start_x += 1
                        start_y -= 1
                        location = str(start_x)+str(start_y)
                        if board_frames[location].piece_is_present == True:
                            return False
                return True
            elif abs(final_y - initial_y) == limit:
                return True
        return False
    
    def knight_move(self, initial_x, initial_y, final_x, final_y):
        if abs(initial_x - final_x) == 1:
            if abs(initial_y - final_y) == 2:
                return True
        elif abs(initial_y - final_y) == 1:
            if abs(initial_x - final_x) == 2:
                return True
        return False

class Knight:
    def valid_move(self, initial_x, initial_y, final_x, final_y):
        move = Pieces().knight_move(initial_x, initial_y, final_x, final_y)
        if move:
            return True
        return False

class Bishop:
    def valid_move(self, initial_x, initial_y, final_x, final_y):
        move = Pieces().diagonal_move(initial_x, initial_y, final_x, final_y)
        if move:
            return True
        return False

class Rook:
    def valid_move(self, initial_x, initial_y, final_x, final_y):
        move1 = Pieces().horizonatal_move(initial_x, initial_y, final_x, final_y)
        move2 =Pieces().vertical_move(initial_x, initial_y, final_x, final_y)
        if move1 or move2:
            return True
        return False

class Queen:
    def valid_move(self, initial_x, initial_y, final_x, final_y):
        move1 = Pieces().diagonal_move(initial_x, initial_y, final_x, final_y)
        move2 = Pieces().horizonatal_move(initial_x, initial_y, final_x, final_y)
        move3 = Pieces().vertical_move(initial_x, initial_y, final_x, final_y)
        if move1 or move2 or move3:
            return True
        return False
    
class King:
    def valid_move(self, initial_x, initial_y, final_x, final_y):
        move1 = Pieces().diagonal_move(initial_x, initial_y, final_x, final_y, 1)
        move2 = Pieces().horizonatal_move(initial_x, initial_y, final_x, final_y, 1)
        move3 = Pieces().vertical_move(initial_x, initial_y, final_x, final_y, 1)
        if move1 or move2 or move3:
            return True
        return False

class Pawn:
    def valid_move(self, initial_x, initial_y, final_x, final_y, limit=None):
        if initial_x == 6 or initial_x == 1:
            limit = 2
        else:
            limit = 1
        location = str(initial_x)+str(initial_y)
        final_location = str(final_x)+str(final_y)
        if board_frames[location].piece_color == 'black':
            if initial_x < final_x:
                return False
        else:
            if initial_x > final_x:
                return False
        move1 = Pieces().vertical_move(initial_x, initial_y, final_x, final_y, limit)
        move2 = Pieces().diagonal_move(initial_x, initial_y, final_x, final_y, 1)
        pawn_color = board_frames[location].piece_color
        killed_piece_color = board_frames[final_location].piece_color
        if move1:
            return True
        if move2 and pawn_color != killed_piece_color and killed_piece_color != None:
                return True
        return False
